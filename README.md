# GeoPos

Application permetant de partager simplement sa position géographique de manière anonyme, facilement (des)activable et sans utilisation de logiciels propriétaires (pas de Google Maps).

Exemple d'utilisation : L'utilisateur d'un smartphone souhaite que ses ami-e-s le rejoigne là où il est actuellement. Il utilise l'application pour générer un URL (ex : `http://geoposapp.fr/carte/oiceih1jah0koViade9wee2ya3jii2th`) et peut le partager avec ses ami-e-s. En consultant cet URL, une carte s'affiche sur la position partagée et permet aux ami-e-s de le rejoindre facilement.

## Consignes générales

- Géolocalise un appareil sous Android,
- Respect de l'anonymat,
- Pas besoin de créer compte,
- Sous licence libre,
- Application coté serveur écrit de préférence en Python ou PHP,
- Utilisation de HTTP(S) et JSON pour la communication avec le serveur. Respect du principe REST,
- Le code, la gestion du projet et la documentation doit être centralisé ici avec un ou plusieurs dépôts Git et les Tickets.

## Fonctionnalités

![Architecture](archi.png)

### Service Android

- Récupération de la position géographique de l'appareil (latitude/longitude/altitude) à interval régulier (~30s) et en tâche de fond
- Envoi de la position obtenue au serveur avec la méthode POST de HTTP
- Activation/désativation du service facilement sur l'appareil. Affichage du statut (actif/inactif) bien visible

### Service de Géolocalisation (Backend)

- Service HTTP recevant la position géograpique de l'appareil
- Enregistrement de la position de l'appareil dans une base de données en conservant l'historique horodaté
- À la demande de l'appareil géolocalisé, génération d'un URL avec un identifiant unique très difficilement devinable. Ex : `http://geoposapp.fr/oiceih1jah0koViade9wee2ya3jii2th`. Cet identifiant doit être relié à l'appareil dans la base de données.
- Sur une requete GET à cet URL, le service fournis la position géographique courante de l'appareil géolocalisé avec l'heure, rien de plus. 

### Frontend

- Page web affichant une carte (Openstreetmap) avec la position de l'appareil. L'URL doit être `http://geoposapp.fr/carte/<id>`
- Page web affichant des statistiques globaux de tous les appareils qui ont envoyés leurs positions :
    - Vitesse moyenne
    - Dénivelé
    - Zone de déplacement (avec dégradé de couleur selon les zones les plus parcourus)

## Bonus
- Version iOS
- Utilisation de plusieurs serveurs de géolocalisation
- Mot de passe pour acceder à la position
- Getion de flotte